INSTALLATION
============

```bash
git clone git@gitlab.com:KOflyan/simple_video_streamer.git && cd simple_video_streamer

npm i
```


DEPLOYMENT
==========
    
Prod:

```bash
npm start
```

Dev:

```bash
npm run start:dev
```
    
    
    