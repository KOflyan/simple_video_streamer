import { Module, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './controller/controller';
import { AppService } from './service/service';
import { StreamWebSocketGateway } from './gateway/stream_socket';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, StreamWebSocketGateway],
})
export class AppModule {
    // configure(consumer: MiddlewareConsumer) : void {
    //     consumer
    //       .apply(RequestMiddleware);
    //   }
}
