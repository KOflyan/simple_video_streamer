
import {
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    WsResponse,
    OnGatewayConnection,
    OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Client, Server } from 'socket.io';
import * as fs from 'fs';
import * as path from 'path';


class ClientData {
    client : Client;
    stream : fs.WriteStream;

    constructor(client: Client, stream: fs.WriteStream) {
        this.client = client;
        this.stream = stream;
    }
}

@WebSocketGateway(8080)
export class StreamWebSocketGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer()
    private server : Server;
    private streamers : ClientData[] = [];

    handleDisconnect(client: Client) : void {
        console.log(`Client [ ${client.id} ] disconnected`);
        let i = this.findClientIndex(client);
        if (i === -1) {
            return;
        }
        this.streamers[i].stream.end();
        this.streamers.splice(i, 1);
    }

    @SubscribeMessage("stream")
    public onEvent(client : Client, data: any): WsResponse<any> {

        let streamer = this.findClient(client);
        if (!streamer) {
            let stream = fs.createWriteStream(path.join(__dirname, `../../videos/${client.id}.webm`), 'base64');
            stream.on('error', err => console.log(err));
            streamer = new ClientData(client, stream);
            this.streamers.push(streamer);

        }

        streamer.stream.write(data.video);
        this.server.emit('stream_slave', data.frame);

        return data;
    }

    public handleConnection(client : Client) : void {
        console.log(`Client [ ${client.id} ] joined`);
    }

    private findClient(client : Client) : ClientData {
        return this.streamers.find( (c) => c.client === client);
    }

    private findClientIndex(client : Client) : number {
        return this.streamers.findIndex( (c) => c.client === client);
    }
}
