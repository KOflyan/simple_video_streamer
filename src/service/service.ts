import { Injectable } from '@nestjs/common';
import { Response } from 'express';

@Injectable()
export class AppService {

    public sendFile(res : Response, fileName : string) : void {
        return res.sendFile(fileName, {root: 'static'});
    }
}
