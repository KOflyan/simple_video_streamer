import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as path from 'path';
import { ValidationPipe } from '@nestjs/common';
import { WsAdapter } from '@nestjs/platform-ws';


(async () => {
    let app = await NestFactory.create<NestExpressApplication>(AppModule);

    app.setViewEngine('ejs');
    app.useStaticAssets('videos');
    app.useStaticAssets('static');
    app.setBaseViewsDir(path.join(path.dirname(require.main.filename), "../static"));
    app.useGlobalPipes(new ValidationPipe());
    app.enableCors({
        origin: [
            'http://localhost:8080'
        ]
    });

    await app.listen(3000);

})();
