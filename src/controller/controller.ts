
import { Controller, Get, Res, Inject, Render, Query } from '@nestjs/common';
import { AppService } from '../service/service';
import { Response } from 'express';
import * as fs from 'fs';
import * as path from 'path';

@Controller()
export class AppController {

    @Inject()
    service : AppService;

    @Get('/')
    @Render('index')
    public home() : any {
        return { files: fs.readdirSync(path.join(__dirname, '../../videos')) };
    }

    @Get('/visualize')
    public visualize(@Res() res : Response) : void {
        return this.service.sendFile(res, 'visualize.html');
    }

    @Get('/stream')
    public stream(@Res() res : Response) : void {
        return this.service.sendFile(res, 'emit.html');
    }

    @Get('/download')
    public download(@Query('id') id : string, @Res() res : Response) : void {
        return res.download(path.join(__dirname, `../../videos/${id}`));
    }

    @Get('/view')
    @Render('view')
    public view(@Query('video') video : string) : any {
        return { video: video };
    }
}
